package com.pixolut.sls;

import act.boot.app.RunApp;
import com.mashape.unirest.http.Unirest;
import org.osgl.mvc.annotation.PostAction;
import act.app.*;

public class App {

    @PostAction("/")
    public String slsProxy(String _body, ActionContext context) throws Exception {
        context.resp().header("Access-Control-Allow-Origin", "*");
        return Unirest.post("https://www.surfguard.slsa.asn.au/SLSA_WebServices/modules/entities/entityFunctions.php").body(_body).asString().getBody();
    }

    public static void main(String[] args) throws Exception {
        RunApp.start(App.class);
    }

}
