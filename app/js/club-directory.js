$(function(){

  var entities = [];
  var googleApiKey = "AIzaSyBE2-feQyqSr-kxXForYZbGt7y7VQY00R8";
  var url = "/proxy/";
  //var url = "http://localhost:5460";

  $("#btn-cd-search").click(function(){
    $("#cd-result").show();
    $("#cd-detail").hide();
    soap();
  });

  $("#cd-query").keypress(function(e) {
    if(e.which == 13) {
      $("#cd-result").show();
      $("#cd-detail").hide();
      soap();
    }
  })

  $("#cd-list").on('click', '.cd-entity .name', function() {
    $("#cd-loading").show();

    var id = $(this).data("id")
    var entity = (_.filter(entities, function(n) {
          return n.EntityID == id;
        }))[0];

    getParent(entity.ParentEntityID).then(function(response) {
      entity.ParentEntityName = response;
      showDetail(entity);
      $("#cd-loading").hide();
    });

  });

  $("#cd-ddl ul").on('click', 'li', function() {
    $("#cd-ddl button .state").text($(this).text());
  });

  $("#cd-back").click(function(){
    $("#cd-result").show();
    $("#cd-detail").hide();
  });

  function soap() {
    $("#cd-loading").show();
    if(entities.length == 0) {
      $.ajax({
        type: 'POST',
        url: url,
        data: '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:entityFunctions.wsdl" xmlns:ent="https://www.surfguard.slsa.asn.au/SLSA_WebServices/wsdl/entityFunctions.wsdl"><soapenv:Header><urn:ctAuthentication><username>pixolut</username><password>P1x0lutW3b5ervice!#</password></urn:ctAuthentication></soapenv:Header><soapenv:Body><ent:getEntities><EntityTypeIDs>4</EntityTypeIDs><EntityID></EntityID><BranchEntityID></BranchEntityID><StateEntityID></StateEntityID><IncludeSLS>1</IncludeSLS><IncludeAcademy>0</IncludeAcademy><IncludeLifeguards>1</IncludeLifeguards><IncludeSupportOperations>0</IncludeSupportOperations><IncludeOtherSLS>0</IncludeOtherSLS><IncludeOrgDetails>1</IncludeOrgDetails><IncludeQLD>1</IncludeQLD><IncludeJoinPublic>1</IncludeJoinPublic><IncludeUpdateInLSO>1</IncludeUpdateInLSO></ent:getEntities></soapenv:Body></soapenv:Envelope>',
        contentType: 'text/plain',
        dataType: 'xml'
      }).done(function(response) {

        entities = [];
        var xml = parseXml(response);
        var entitiesXML = xml.getElementsByTagName('Entity');

        getEntities(entitiesXML);

      }).always(function() {
        $("#cd-loading").hide();
      });

    } else {
      var html = "";

      _.forEach(entities, function(entity) {
        if(filterEntity(entity)) {
          var item = '<div class="cd-entity"><span class="name" data-id="' + entity.EntityID + '">' + _.capitalize(entity.DisplayName) + '</span><span class="address">' + entity.PhysicalAddress + ', ' + entity.PhysicalState + ' ' + entity.PhysicalPostCode + '</span></div>';
          html += item;
        }
      });

      $("#cd-list").html(html);
      $("#cd-loading").hide();
    }
  }

  function getEntities(xml) {
    var html = "";
    entities = [];

    _.forEach(xml, function(e){
      var entity = {
        'EntityID': e.getAttribute('EntityID'),
        'ParentEntityID': e.getAttribute('ParentEntityID'),
        'DisplayName': e.getElementsByTagName('DisplayName')[0].textContent,
        'OfficePhone': e.getElementsByTagName('OfficePhone')[0].textContent,
        'OfficeFax': e.getElementsByTagName('OfficeFax')[0].textContent,
        'EmailAddress': e.getElementsByTagName('EmailAddress')[0].textContent,
        'WebSite': e.getElementsByTagName('WebSite')[0].textContent,
        'PhysicalAddress': e.getElementsByTagName('PhysicalAddress')[0].textContent,
        'PhysicalSuburb': e.getElementsByTagName('PhysicalSuburb')[0].textContent,
        'PhysicalState': e.getElementsByTagName('PhysicalState')[0].textContent,
        'PhysicalPostCode': e.getElementsByTagName('PhysicalPostCode')[0].textContent,
        'ClubhouseBeachName': e.getElementsByTagName('ClubhouseBeachName')[0].textContent,
        'ClubhouseBeachKey': e.getElementsByTagName('ClubhouseBeachKey')[0].textContent
      };

      entities.push(entity);

      if(filterEntity(entity)) {
        var item = '<div class="cd-entity"><span class="name" data-id="' + entity.EntityID + '">' + _.capitalize(entity.DisplayName) + '</span><span class="address">' + entity.PhysicalAddress + ', ' + entity.PhysicalState + ' ' + entity.PhysicalPostCode + '</span></div>';
        html += item;
      }
    });

    $("#cd-list").html(html);
    $("#cd-loading").hide();
  }

  function getParent(id) {
    return $.ajax({
      type: 'POST',
      url: url,
      data: '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:entityFunctions.wsdl" xmlns:ent="https://www.surfguard.slsa.asn.au/SLSA_WebServices/wsdl/entityFunctions.wsdl"><soapenv:Header><urn:ctAuthentication><username>pixolut</username><password>P1x0lutW3b5ervice!#</password></urn:ctAuthentication></soapenv:Header><soapenv:Body><ent:getEntities><EntityTypeIDs></EntityTypeIDs><EntityID>'
            + id
            + '</EntityID><BranchEntityID></BranchEntityID><StateEntityID></StateEntityID><IncludeSLS>1</IncludeSLS><IncludeAcademy>0</IncludeAcademy><IncludeLifeguards>1</IncludeLifeguards><IncludeSupportOperations>0</IncludeSupportOperations><IncludeOtherSLS>0</IncludeOtherSLS><IncludeOrgDetails>0</IncludeOrgDetails><IncludeQLD>1</IncludeQLD><IncludeJoinPublic>1</IncludeJoinPublic><IncludeUpdateInLSO>1</IncludeUpdateInLSO></ent:getEntities></soapenv:Body></soapenv:Envelope>',
      contentType: 'text/plain',
      dataType: 'xml'
    }).then(function(response) {

      var xml = parseXml(response);;
      var parentName =  xml.getElementsByTagName('Entity')[0].textContent;
      return parentName;

    });
  }

  function parseXml(xml) {
    var data = xml.getElementsByTagName('output')[0].textContent;
    return $.parseXML(data);
  }

  function showDetail(entity) {
    $("#cd-result").hide();
    $("#cd-map").attr("src", "");

    var adressEncode = encodeURIComponent(entity.PhysicalAddress + " " + entity.PhysicalSuburb + " " + entity.PhysicalState + " " + entity.PhysicalPostCode);
    var mapSrc = "https://www.google.com/maps/embed/v1/place?q=" + adressEncode + "&key=" + googleApiKey;
    $("#cd-name").text(entity.DisplayName);
    $("#cd-map").attr("src", mapSrc);
    $("#cd-parent .detail").text(entity.ParentEntityName);
    $("#cd-phone .detail").text(entity.OfficePhone);
    $("#cd-fax .detail").text(entity.OfficeFax);
    $("#cd-email .detail").html('<a href="mailto:' + entity.EmailAddress + '" target="_blank">' + entity.EmailAddress + '</a>');
    $("#cd-website .detail").html('<a href="http://' + entity.WebSite + '" target="_blank">' + entity.WebSite + '</a>');
    $("#cd-address .detail").text(entity.PhysicalAddress);
    $("#cd-suburb .detail").text(entity.PhysicalSuburb);
    $("#cd-state .detail").text(entity.PhysicalState);
    $("#cd-postcode .detail").text(entity.PhysicalPostCode);
    $("#cd-beach .detail").html('<a href="https://beachsafe.org.au/beach/' + entity.ClubhouseBeachKey +  '" target="_blank">' + entity.ClubhouseBeachName + '</a>');

    $("#cd-detail").show();
  }

  function filterEntity(entity) {
    var query = $("#cd-query").val().trim();
    var state = $("#cd-ddl button .state").text();

    if(state === "All States" || state === "Unknown") {
      state = true;
    } else if(entity.PhysicalState === state) {
      state = true;
    } else {
      state = false;
    }

    return ((entity.DisplayName.toLowerCase().indexOf(query.toLowerCase()) !== -1 && state ) 
      || (entity.ClubhouseBeachName.toLowerCase().indexOf(query.toLowerCase()) !== -1 && state)
      || (entity.PhysicalSuburb.toLowerCase().indexOf(query.toLowerCase()) !== -1 && state)
      || (entity.PhysicalPostCode == query && state))
  }
});
